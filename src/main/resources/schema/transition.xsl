<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fields="http://rocketscien.se/test/fields"
                xmlns:attrs="http://rocketscien.se/test/attributes">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <attrs:entries>
            <xsl:apply-templates/>
        </attrs:entries>
    </xsl:template>

    <xsl:template match="fields:entry">
        <attrs:entry>
            <xsl:attribute name="field">
                <xsl:value-of select="fields:field"/>
            </xsl:attribute>
        </attrs:entry>
    </xsl:template>
</xsl:stylesheet>