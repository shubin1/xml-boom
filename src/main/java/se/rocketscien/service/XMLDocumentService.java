package se.rocketscien.service;

import lombok.RequiredArgsConstructor;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import se.rocketscien.model.Test;
import se.rocketscien.service.converter.TestConverter;
import se.rocketscien.test.fields.Entries;
import se.rocketscien.test.fields.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXResult;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@RequiredArgsConstructor
public class XMLDocumentService {

    private final TestConverter testConverter;

    public void marshallTestsIntoFile(List<Test> tests, File destination) throws JAXBException {
        ObjectFactory objectFactory = new ObjectFactory();
        JAXBContext context = JAXBContext.newInstance(Entries.class);
        Marshaller marshaller = context.createMarshaller();

        marshaller.marshal(objectFactory.createEntries(testConverter.convertToEntries(tests)), destination);
    }

    public void transformFileWithXslTemplateToFile(File source, URL templateURL, File destination) throws IOException, TransformerException, XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newFactory();
        XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();

        XMLStreamReader templateStreamReader = inputFactory.createXMLStreamReader(templateURL.openStream());
        StAXSource stAXSource = new StAXSource(templateStreamReader);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Templates xsl = transformerFactory.newTemplates(stAXSource);
        Transformer transformer = xsl.newTransformer();

        XMLStreamReader sourceStreamReader = inputFactory.createXMLStreamReader(new FileReader(source));
        XMLStreamWriter destinationStreamWriter = outputFactory.createXMLStreamWriter(new FileWriter(destination));

        transformer.transform(new StAXSource(sourceStreamReader), new StAXResult(destinationStreamWriter));
    }

    public String evaluateXPathExpressionOnFile(File source, String expression) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document parsedSource = builder.parse(source);

        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();

        XPathExpression compiled = xPath.compile(expression);

        return compiled.evaluate(parsedSource);
    }

}
