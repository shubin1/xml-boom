package se.rocketscien.service.converter;

import se.rocketscien.model.Test;
import se.rocketscien.test.fields.Entries;
import se.rocketscien.test.fields.Entry;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

public class TestConverter {

    public Entries convertToEntries(List<Test> tests) {
        List<Entry> collectedEntries = tests.stream()
                .map(t -> {
                    Entry entry = new Entry();
                    entry.setField(BigInteger.valueOf(t.getField()));
                    return entry;
                })
                .collect(Collectors.toList());

        Entries entries = new Entries();
        entries.getEntry().addAll(collectedEntries);

        return entries;
    }

}
