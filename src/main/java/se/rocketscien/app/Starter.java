package se.rocketscien.app;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.sqlite.SQLiteConfig;
import org.sqlite.javax.SQLiteConnectionPoolDataSource;
import se.rocketscien.dao.TestRepository;
import se.rocketscien.dao.mapper.TestMapper;
import se.rocketscien.model.Test;
import se.rocketscien.service.XMLDocumentService;
import se.rocketscien.service.converter.TestConverter;

import javax.sql.DataSource;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Setter
@Getter
public class Starter {

    private static final String SCHEMA_TRANSITION_XSL = "/schema/transition.xsl";
    private static final String FIRST_FILE_NAME = "out/1.xml";
    private static final String SECOND_FILE_NAME = "out/2.xml";

    private String dbUrl;

    private Long number;

    public void run() throws Exception {
        TestConverter testConverter = new TestConverter();
        TestRepository testRepository = new TestRepository(acquireSessionFactory());
        XMLDocumentService xmlDocumentService = new XMLDocumentService(testConverter);

        testRepository.createTableIfNotExists();

        File firstXml = new File("out/1.xml");
        File secondXml = new File("out/2.xml");

        // Clean up
        testRepository.deleteAllTests();

        // Insert
        List<Test> tests = LongStream.range(1, number + 1).boxed()
                .map(Test::new)
                .collect(Collectors.toList());
        testRepository.insertAllTests(tests);

        // Read and Marshall
        List<Test> allTests = testRepository.getAllTests();

        xmlDocumentService.marshallTestsIntoFile(allTests, firstXml);

        // XSLT transform
        xmlDocumentService.transformFileWithXslTemplateToFile(firstXml, getClass().getResource(SCHEMA_TRANSITION_XSL), secondXml);

        // Summation
        String evaluated = xmlDocumentService.evaluateXPathExpressionOnFile(secondXml, "sum(//entry/@field)");

        System.out.println(String.format("Evaluated sum: %s", evaluated));
    }

    private SqlSessionFactory acquireSessionFactory() {
        DataSource dataSource = acquireDataSource(dbUrl);
        JdbcTransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment main = new Environment("main", transactionFactory, dataSource);

        Configuration configuration = new Configuration(main);
        configuration.addMapper(TestMapper.class);

        return new SqlSessionFactoryBuilder().build(configuration);
    }

    private DataSource acquireDataSource(String dbUrl) {
        SQLiteConfig sqliteConfig = new SQLiteConfig();
        SQLiteConnectionPoolDataSource sqliteDataSource = new SQLiteConnectionPoolDataSource(sqliteConfig);

        sqliteDataSource.setUrl(dbUrl);

        return sqliteDataSource;
    }

}
