package se.rocketscien;

import se.rocketscien.app.Starter;

import java.time.Instant;

public class App {
    public static void main(String[] args) throws Exception {
        String dbUrl = args[0];
        String number = args[1];

        Starter starter = new Starter();
        starter.setDbUrl(dbUrl);
        starter.setNumber(Long.parseLong(number));

        Instant start = Instant.now();
        starter.run();
        Instant end = Instant.now();

        double executedSeconds = (end.toEpochMilli() - start.toEpochMilli()) / 1000d;
        System.out.println(String.format("Simplest time measurement: %f seconds", executedSeconds));
    }
}
