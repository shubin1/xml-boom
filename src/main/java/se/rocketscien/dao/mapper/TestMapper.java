package se.rocketscien.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import se.rocketscien.model.Test;

import java.util.List;

@Mapper
public interface TestMapper {

    void insertTest(long number);

    List<Test> selectAll();

    void deleteAll();

    void createTableIfNotExists();
}
