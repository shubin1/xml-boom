package se.rocketscien.dao;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import se.rocketscien.dao.mapper.TestMapper;
import se.rocketscien.model.Test;

import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class TestRepository {

    private final SqlSessionFactory sessionFactory;

    public List<Test> getAllTests() {
        return executeInsideSession(TestMapper::selectAll);
    }

    public void insertTest(Test test) {
        executeInsideSession((Function<TestMapper, Void>) mapper -> {
            mapper.insertTest(test.getField());

            return null;
        });
    }

    public void insertAllTests(List<Test> tests) {
        executeInsideSession((Function<TestMapper, Void>) mapper -> {
            tests.stream()
                    .map(Test::getField)
                    .forEach(mapper::insertTest);

            return null;
        });
    }

    public void createTableIfNotExists() {
        executeInsideSession((Function<TestMapper, Void>) mapper -> {
            mapper.createTableIfNotExists();

            return null;
        });
    }

    public void deleteAllTests() {
        executeInsideSession((Function<TestMapper, Void>) mapper -> {
            mapper.deleteAll();
            return null;
        });
    }

    private <T> T executeInsideSession(Function<TestMapper, T> func) {
        try (SqlSession session = sessionFactory.openSession()) {
            TestMapper mapper = session.getMapper(TestMapper.class);

            T result = func.apply(mapper);

            session.flushStatements();
            session.commit();
            return result;
        }
    }

}
